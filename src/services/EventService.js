import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 3000
})

export default {
  getEvents(howManyPerPage, pageToStartWith) {
    return apiClient.get(
      '/events?_limit=' + howManyPerPage + '&_page=' + pageToStartWith
    )
  },
  getEventById(id) {
    return apiClient.get('/events/' + id)
  },
  postEvent(event) {
    return apiClient.post('/events', event)
  }
}
