import Vue from 'vue'
import Vuex from 'vuex'
import * as userModule from '@/store/modules/user.js'
import * as eventModule from '@/store/modules/event.js'
import * as notifModule from '@/store/modules/notification.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    userModule,
    eventModule,
    notifModule
  },
  state: {
    categories: [
      'sustainability',
      'nature',
      'animal welfare',
      'housing',
      'education',
      'food',
      'community'
    ],
  },
  getters: {
    getHowManyCategories: state => {
      return state.categories.length
    },
  }
})
