export const namespaced = true

export const state = {
  notifications: []
}

let nextId = 1

export const mutations = {
  PUSH(state, notificationToAdd) {
    state.notifications.push({
      ...notificationToAdd,
      id: nextId++
    })
  },
  DELETE(state, notificationToDelete) {
    state.notifications = state.notifications.filter(notification => notification.id !== notificationToDelete.id)
  }
}

export const actions = {
  add({ commit }, notificationToAdd) {
    commit('PUSH', notificationToAdd)
  },
  remove({ commit }, notificationToDelete) {
    commit('DELETE', notificationToDelete)
  }
}