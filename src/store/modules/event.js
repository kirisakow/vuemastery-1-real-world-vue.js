import EventService from '@/services/EventService.js'

export const namespaced = true

export const state = {
  events: [],
  eventsTotal: 0,
  eventToShow: {},
  howManyPerPage: 3
}

export const mutations = {
  ADD_EVENT(state, event) {
    state.events.push(event)
  },
  SET_EVENTS(state, events) {
    state.events = events
  },
  SET_EVENTS_TOTAL(state, eventsTotal) {
    state.eventsTotal = eventsTotal
  },
  SET_EVENT_TO_SHOW(state, eventToShow) {
    state.eventToShow = eventToShow
  }
}

export const actions = {
  addEventAsync({ commit, dispatch }, event) {
    return EventService.postEvent(event)
      .then(() => {
        commit('ADD_EVENT', event)
        dispatch(
          'notifModule/add',
          {
            type: 'success',
            message: 'addEventAsync OK'
          },
          { root: true }
        )
        return event
      })
      .catch(error => {
        dispatch(
          'notifModule/add',
          {
            type: 'error',
            message: 'Error occurred in addEventAsync: ' + error.message
          },
          { root: true }
        )
        throw error
      })
  },
  fetchEventsAsync({ commit, dispatch, state }, { pageToStartWith }) {
    return EventService.getEvents(state.howManyPerPage, pageToStartWith)
      .then(response => {
        commit('SET_EVENTS_TOTAL', response.headers['x-total-count'])
        commit('SET_EVENTS', response.data)
      })
      .catch(error => {
        dispatch(
          'notifModule/add',
          {
            type: 'error',
            message: 'Error occurred in fetchEventsAsync: ' + error.message
          },
          { root: true }
        )
      })
  },
  fetchEventByIdAsync({ commit, getters }, id) {
    var eventToShow = getters.getEventById(id)
    if (eventToShow) {
      commit('SET_EVENT_TO_SHOW', eventToShow)
      return eventToShow
    } else {
      return EventService.getEventById(id)
        .then(response => {
          commit('SET_EVENT_TO_SHOW', response.data)
          return response.data
        })
    }
  }
}

export const getters = {
  getEventById: state => id => {
    return state.events.find(event => id === event.id)
  }
}
